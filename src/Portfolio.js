function load (){
	setTimeout(function () {
		document.getElementById("loader").style.display = "none";
		document.getElementsByClassName("banner")[0].style.visibility = "visible";
		document.getElementsByClassName("social-media")[0].style.visibility = "visible";}, 2000);
}



window.onscroll = function () {
	scrollFunction()
};

function scrollFunction() {
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		document.getElementById("onTop").style.display = "block";
	} else {
		document.getElementById("onTop").style.display = "none";
	}
}

function topFunction() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}

function showOverlay() {
	let projectScreen = document.querySelector(".project");
	projectScreen.forEach(elem => elem.addEventListener("touchstart", function(){
		elem.classList.add('project-overlay')
	}))
}
